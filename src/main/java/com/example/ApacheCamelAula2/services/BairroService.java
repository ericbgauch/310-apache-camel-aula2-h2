package com.example.ApacheCamelAula2.services;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;


import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.ApacheCamelAula2.Repository.BairroRepository;
import com.example.ApacheCamelAula2.entidades.Bairro;
import com.example.ApacheCamelAula2.modelos.ClienteImportacao;

@Service
public class BairroService {

	
	@Autowired
	private BairroRepository bairroRepository;
	
	//Este método tem que ser transacional para conseguir persistir no banco.
	@Transactional
	public void importarBairro(File arquivo) throws IOException {
		
		CSVParser csv = CSVFormat.DEFAULT.parse(new FileReader(arquivo));

		for (CSVRecord linha : csv) {
			Bairro bairro = new Bairro();
			bairro.setNome(linha.get(0));
			salvarBairro(bairro);
		}
	}
	
	public void salvarBairro(Bairro bairro) {
		bairroRepository.save(bairro);
	}
}